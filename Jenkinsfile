def slackMessage() {
    def JENKINS_IP = "52.44.172.97"
    def JENKINS_PORT = "8080"
    def JENKINS_BUILD_URL = "http://${JENKINS_IP}:${JENKINS_PORT}/job/${env.JOB_NAME}/${env.BUILD_ID}/"
    if (currentBuild.result == 'FAILURE') {
        def JENKINS_LOG= "FAILED: the job:'${env.JOB_NAME}' build of number '${env.BUILD_NUMBER}' has been failed. Logs path: ${JENKINS_BUILD_URL}console"
        return JENKINS_LOG
    } else {
        def JENKINS_LOG = "SUCCEEDED: the job:'${env.JOB_NAME}' build of number '${env.BUILD_NUMBER}' has been deployed successfully. | Build URL => (<${JENKINS_BUILD_URL}|Build URL>)"
        return JENKINS_LOG
    }
}
pipeline {
    agent any
    environment {
        PROMETHEUS_TOWER = "75.101.145.46"
        ANSIBLE_CONTROLLER = "34.204.207.236"
        ANSIBLE_CONTROLLER_HOME_DIR = "/home/ubuntu"
        IMAGE_NAME = "hamzalouis/devops-demo:node-${env.BUILD_ID}"
    }
    stages {

        stage ("build/push Docker image") {
            steps {
                script{
                    echo "Building the Docker image ..."
                    withCredentials([usernamePassword(credentialsId: 'docker-repo', passwordVariable: 'pass', usernameVariable: 'user')]){
                        sh "docker build -t ${IMAGE_NAME} ."
                        sh "echo ${pass} | docker login -u ${user} --password-stdin"
                        sh "docker push ${IMAGE_NAME}"
                    }
                }
            }
        }
        stage ("Provision infrastructure with Terraform") {
            environment {
                AWS_ACCESS_KEY_ID= credentials('AWS_ACCESS_KEY_ID_JENKINS')
                AWS_SECRET_ACCESS_KEY= credentials('AWS_SECRET_ACCESS_KEY_JENKINS')
                AWS_SESSION_TOKEN= credentials('AWS_SESSION_TOKEN_JENKINS')
            }
            steps {
                script {
                    dir ('terraform') {
                      
                        sh "terraform init"
                        sh "terraform apply --auto-approve"
                        sh "terraform output node_server_public_ip"
                    }
                }
            }
        }
        stage ("Transfer files to the ansible-controller") {
            steps {
                script {
                    echo "Transfering ansible files from the repo to the ansible controller ..."
                    dir ('ansible') {
                        sh "echo IMAGE_NAME: ${IMAGE_NAME} > DOCKER_IMAGE"
                    }
                    sleep(time: 5, unit:"SECONDS")
                    sshagent(['ansible-controller-key']) {
                        sh "scp -o StrictHostKeyChecking=no ansible/* ubuntu@${ANSIBLE_CONTROLLER}:${ANSIBLE_CONTROLLER_HOME_DIR}"
                         withCredentials([sshUserPrivateKey(credentialsId: 'ansible-slaves-key', keyFileVariable: 'sshPrivateKey', usernameVariable: 'user')]) {
                            sh 'scp ${sshPrivateKey} ubuntu@${ANSIBLE_CONTROLLER}:${ANSIBLE_CONTROLLER_HOME_DIR}/ssh-key.pem'  
                        }
                    }
                }
            }
        }
        stage("Execute the main playbook.yaml") {
            steps {
                script {
                    sleep(time: 90, unit:"SECONDS")
                    echo "Transfering and executing the ansible-playbook script file on the provisioned server ..."
                    def shellCmd = "bash ./ansible-playbook.sh"
                    sshagent(['ansible-controller-key']) {
                        sh "ssh -o StrictHostKeyChecking=no ubuntu@${ANSIBLE_CONTROLLER} ${shellCmd}"
                    }
                }
            }
        }
        stage("Expose the /metrics path using node_exporter.yaml ") {
            steps {
                script {
                    echo "Deploying node_exporter container on the server to expose the OS resources metrics ..."
                    def shellCmdNodeExporter = "bash ./ansible-playbook-node-exporter.sh"
                    sshagent(['ansible-controller-key']) {
                        sh "scp -o StrictHostKeyChecking=no ansible/ansible-playbook-node-exporter.sh ubuntu@${ANSIBLE_CONTROLLER}:${ANSIBLE_CONTROLLER_HOME_DIR}"
                        sh "ssh -o StrictHostKeyChecking=no ubuntu@${ANSIBLE_CONTROLLER} ${shellCmdNodeExporter}"
                    }
                }
            }
        }
        stage("Configure the IP in the prometheus.yaml") {
            steps {
                script {
                    echo "Executing the change-ip.sh script to insert the IP of the server in prometheus configurations ..."
                    def IP_ADDRESS = input message: 'Please enter the value the IP address of the server', parameters: [string(defaultValue: '', description: 'IP address of the server', name: 'IP_ADDRESS')]
                    def shellCmdIpChanging = "bash ./change-ip.sh"
                    sshagent(['prometheus-tower-key']) {
                        sh "scp -o StrictHostKeyChecking=no prometheus/change-ip.sh ec2-user@${PROMETHEUS_TOWER}:/home/ec2-user"
                        sh "ssh -o StrictHostKeyChecking=no ec2-user@${PROMETHEUS_TOWER} ${shellCmdIpChanging} ${IP_ADDRESS}"
                    }
                }
            }
        }
    }
    post {
        always {
            echo "A notification with the results of this build has been sent to the slack channel"
        }
        success {
            slackSend (color:"good", message:"${slackMessage()}")
        }
        failure {
            slackSend (color:"danger", message: "${slackMessage()}")
        }
    }
}