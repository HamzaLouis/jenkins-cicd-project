#!/bin/bash
cd ~/config/prometheus
server_ip=$1
sed -i "s/ip/${server_ip}/g" prometheus.yaml
echo ${server_ip}
sudo systemctl start docker
sudo systemctl status docker
sudo docker ps -a | grep "Exited" | awk '{print $NF}' | xargs sudo docker start 2> /dev/null
