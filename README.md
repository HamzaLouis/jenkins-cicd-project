<div>
<h1 align="center">Author : Hamza Louis :surfer:</h1>
<h4 align="center">A motivated DevOps engineer. In love with scripting , automation and making releases as fast as possible :curly_loop: </h4>


<p><br><br></p>
</div>
<div>
<h3 align="center">Technologies / Tools used:</h3>
<p><br></p>
<p align="center"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/redhat/redhat-plain.svg" alt="redhat" width="70" height="70"/> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/debian/debian-plain-wordmark.svg" alt="debian" width="70" height="70"/> <a href="https://aws.amazon.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/amazonwebservices/amazonwebservices-plain-wordmark.svg" alt="aws" width="70" height="70"/> </a> <a href="https://www.gnu.org/software/bash/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bash/bash-original.svg" alt="bash" width="70" height="70"/> </a><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/ansible/ansible-original.svg" alt="Ansible" width="70" height="70"/> <a href="https://www.docker.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-original-wordmark.svg" alt="docker" width="70" height="70"/> </a> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/terraform/terraform-original.svg" alt="terraform" width="70" height="70"/><a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="70" height="70"/> </a>  <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer">  <a href="https://www.jenkins.io" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/jenkins/jenkins-icon.svg" alt="jenkins" width="70" height="70"/> </a><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/prometheus/prometheus-original.svg" alt="prometheus" width="70" height="70"/> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/grafana/grafana-original.svg" alt="grafana" width="70" height="70"/><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="70" height="70"/> </a><a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="70" height="70"/> </a><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/nodejs/nodejs-plain-wordmark.svg" alt="nodejs" width="100" height="100"/> </p>
</div>
<p><br><br></p>
<div align="center">
<h1>Jenkins CI/CD Project</h1>

<p><br></p>

_**This repository is a demo repository, because of that you just see a single branch and a little number of commits. In real projects, with more than one devops engineer interact with the same repo, you will see more number of branches.**_

<p><br></p>

<h1>Project Description</h1>

<div align="left">

<p>The Jenkins CI/CD Project is a DevOps project can be used to deliver a simple nodejs project as fast as possible,this project can be replaced with any larger nodejs project you need to have all needed packages used in the project in the package.json file to be imported in the building process. The developer will push the code to the remote repository, this will trigger a webhook in order to launch the pipeline building process. </p>

<p>This project uses Terraform as the Infrastructure as Code (IaC) technology in order to provide the needed infrastructure to our application on AWS cloud. The developer can test his code by running the application after providing the required infrastructure for testing environment , then the developer can erase all resources on AWS by executing - terraform destroy - as simple as that. Finally , after the developer has validated the functionallity of his application , the developer now able to execute the pipeline again with the needed infrastructure in the production environment.</p>

<p>The project uses jenkins as the main CI/CD tool , jenkins has many alternatives but there are also many advantages for jenkins over other CI/CD technologies. Its advantages : open source tool with huge community so you can get support easily and quickly , easy to install , has hundreds of useful and powerful plugins can be used in the pipeline stages.</p>

<p>The project uses Ansible as the configuration management tool, Ansible can be written in YAML which is considered as the most simple declarative language so you need just to describe the end state of the resource and Ansible will handle the hard work for you. Ansible is agentless configuration management tool so it is easily to configure, you just need to install it on the master node and configure the hosts file or use the dynamic inventory to get more robust Ansible work. </p>

</div>

<p><br></p>

<h2>Project Files Navigator :</h2>

<div align="left">

<p><br></p>

+ [Nodejs files](app)
+ [Ansible files](ansible)
+ [Terraform files](terraform)
+ [Prometheus files](prometheus)
+ [Dockerfile](Dockerfile)
+ [Jenkinsfile](Jenkinsfile)


</div>

<p><br></p>


<h3>Preconfigured EC2 Instances : </h3>

<p><br></p>

| EC2 instance name | Functionallity 
| ---      | ---      
| Jenkins   | It has jenkins on it , the CI/CD tool used in this project   
| Ansible-Controller | It has the Ansible binaries to execute the playbooks 
| Prometheus-Tower   | It has the prometheus and grafana containers to monitor the application          

</div>

<p><br></p>

<h2>How to configure the Ansible-Controller on an Ubuntu VM ?</h2>

<div align="center">
<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/ansible/ansible-original.svg" alt="Ansible" width="100" height="100"/> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/ubuntu/ubuntu-plain-wordmark.svg" alt="Ubuntu" width="100" height="100"/>
<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="Python" width="100" height="100"/>
</div>

<p><br></p>

<p>Note that you need to have python already installed on the VM , also the managed / slaves VM's of the Ansible master need to have python already installed since it is the first thing that the playbook needs to validate on the slave side. </p>

<p><br></p>

```bash
sudo apt update
sudo apt upgrade
sudo apt -y install software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt install ansible
ansible --version
```

<p><br></p>
<h2>Docker Containers Commands :</h2>

<h3>Jenkins Container</h3>

<a href="https://www.jenkins.io" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/jenkins/jenkins-icon.svg" alt="jenkins" width="70" height="70"/>

```bash
docker run -p 8080:8080 -p 50000:50000 -d/ 
-v jenkins_home:/var/jenkins_home /
-v /var/run/docker.sock:/var/run/docker.sock /
-v $(which docker):/usr/bin/docker jenkins/jenkins:lts

```



<h3>Prometheus Container</h3>

<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/prometheus/prometheus-original.svg" alt="prometheus" width="70" height="70"/>

```bash
docker run -d  -p 9090:9090 -v /srv/Config/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus

```

<h3>Grafana Container</h3>

<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/grafana/grafana-original.svg" alt="grafana" width="70" height="70"/>

```bash
docker run -d --name=grafana -p 3456:3000 grafana/grafana

```
<h3>Node Expoter Container</h3>

```bash
docker run -d --pid="host"  -v "/:/host:ro,rslave" -p 9100:9100 prom/node-exporter:latest --path.rootfs=/host

```

<p><br></p>

<div>
<h1 align="center">Project Workflow :</h1> 

1. The developer push the changes to the remote repository.
2. As a result , the jenkins pipeline will be triggered by a webhook configured with gitlab.
3. Jenkins will fetch all the files from the source repository , then it will begin the execution of all stages.
4. The first stage will build and push the docker container to the docker registry, npm will be used as a package manager for the nodejs application.
5. The next stage will provide the infrastructure on AWS cloud using terraform ,Infrastructure as code, (IaC) technology.
6. The next stage will use the Ansible configuration management tool to provide all dependencies and configurations needed in order to deploy our application.
7. The next stage will provide the node exporter in order to expose the /metrics path for our EC2 instance to be able to fetch and monitor all metrics.
8. The final stage will configure the prometheus-tower EC2 instance, by configuring the prometheus.yml file and then by starting the prometheus and grafana containers.
9. After the execution of all stages completed, a notification consists of a link with the console output and a discription of the result will be sent to a slack channel.
</div>

<p><br><br></p>

<div>
<h1 align="center">Possible Future Modifications :star: :</h1>

<p><br></p>

<div align="center">
<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/kubernetes/kubernetes-plain.svg" alt="kubernetes" width="100" height="100"/>
<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/gitlab/gitlab-original.svg" alt="Gitlab" width="120" height="100"/>
<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/googlecloud/googlecloud-original.svg" alt="Google Cloud" width="100" height="100"/>
</div>


<p><br></p>

+ We have used the Gitlab as a source manager for our code and files, in the next stage we will use it as the main CI/CD tool.

+ We have used Ansible as a configuration management tool for this level of our project , in the next stage we may use the Chef or Puppet configuration management tools.

+ In this project we have deployed a docker container on an EC2 instance , in the next stage we will use either Google Kubernetes Engine (GKE) or Elastic Kubernetes Service (EKS) in order to get more control on our application since it will get more containers and deployments when the code gets more complicated

</div>


<h2>Any recommendations:</h2>

<p> 

- For any recommendations or advice you can reach me at **Hamza.Louis@outlook.com** :blue_heart:
</p>
