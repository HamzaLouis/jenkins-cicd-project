FROM node:19-alpine

RUN mkdir -p /usr/app
WORKDIR /usr/app/

COPY package*.json /usr/app/
RUN npm install

COPY app/* /usr/app/

EXPOSE 3000
CMD ["node", "server.js"]