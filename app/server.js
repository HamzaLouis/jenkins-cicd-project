const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const app = express();

const adminRoute = require("./admin.js");
const mainRoute = require("./main.js");
const successRoute = require("./success.js");

app.use(express.static(path.join(__dirname)));
app.use(bodyParser.urlencoded({ extended: false })); 
app.use(adminRoute);
app.use(mainRoute);
app.use(successRoute);

app.use((req, res, next) => {
  res.status(404).sendFile(path.join(__dirname, "404.html"));
});

app.listen(3000);
