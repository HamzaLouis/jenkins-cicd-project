const express = require("express");
const path = require("path");

const Dir = require("./dir");

const router = express.Router();

router.get("/", (req, res, next) => {
  res.sendFile(path.join(Dir, "main.html"));
});

module.exports = router;
