const express = require("express");
const path = require("path");

const Dir = require("./dir");

const router = express.Router();

router.get("/admin/add-employee", (req, res, next) => {
  res.sendFile(path.join(Dir, "admin.html"));
});

router.post("/admin/add-employee", (req, res, next) => {
  res.redirect("/succeeded");
});

module.exports = router;
