const express = require("express");
const path = require("path");

const Dir = require("./dir");

const router = express.Router();

router.post("/succeeded", (req, res, next) => {
  res.sendFile(path.join(Dir, "success.html"));
});

module.exports = router;
