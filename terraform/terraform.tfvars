vpc_cidr_block = "10.2.0.0/16"
subnet_cidr_block = "10.2.10.0/24"
avail-zone = "us-east-1a"
environment = "deploy"
myip= "192.168.10.70/32"
instance_type = "t2.micro"
image_name = "amzn2-ami-hvm-*-x86_64-gp2"