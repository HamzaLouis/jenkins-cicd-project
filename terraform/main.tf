provider "aws" {
    region = "us-east-1"
}


terraform {

backend "s3" {
    bucket = "tfstate-s3-backup-bucket"
    key    = "tfstates/node-app-infra.tfstate"
    region = "us-east-1"
   
  }

}  

resource "aws_vpc" "node_vpc" {
    cidr_block = var.vpc_cidr_block
    enable_dns_hostnames = true
    tags = {
        Name: "${var.environment}-vpc"
    }
}



module "node_public_subnet" {
  source = "./modules/App-Subnet"
  subnet_cidr_block = var.subnet_cidr_block
  avail-zone = var.avail-zone
  environment = var.environment
  default_route_table_id = aws_vpc.node_vpc.default_route_table_id
  vpc_id = aws_vpc.node_vpc.id
}


module "node_app_server" {
  source = "./modules/App-Server"
  myip = var.myip
  environment = var.environment
  image_name = var.image_name
  instance_type = var.instance_type
  subnet_id = module.node_public_subnet.subnet.id
  avail-zone = var.avail-zone
  vpc_id = aws_vpc.node_vpc.id
}

output "node_server_public_ip" {
  value = module.node_app_server.ec2_pub_ip
}
