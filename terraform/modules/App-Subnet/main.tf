resource "aws_subnet" "node_public_subnet" {
  vpc_id = var.vpc_id
  availability_zone = var.avail-zone
  cidr_block = var.subnet_cidr_block
  tags = {
    Name: "${var.environment}-node-public-subnet"
  }
}

resource "aws_default_route_table" "node_default_RT" {
  default_route_table_id = var.default_route_table_id

  route  {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.node-IG.id
  }
  tags = {
    Name: "${var.environment}-default-RT"
  }
}

resource "aws_internet_gateway" "node-IG" {
  vpc_id = var.vpc_id
  tags = {
    Name: "${var.environment}-IG"
  }
}
