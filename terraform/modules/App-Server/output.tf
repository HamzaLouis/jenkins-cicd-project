output "SecurityGroup" {
  value = aws_default_security_group.default-SG
}


output "ec2-object" {
  value = aws_instance.node_app_ec2
}

output "ec2_pub_ip" {
  value = aws_instance.node_app_ec2.public_ip
}