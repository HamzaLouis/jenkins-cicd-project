resource "aws_default_security_group" "default-SG" {
  vpc_id = var.vpc_id
  

  ingress  {
    cidr_blocks = [ "0.0.0.0/0" ]
    from_port = 22
    protocol = "tcp"
    to_port = 22
  } 

  ingress  {
    cidr_blocks = [ "0.0.0.0/0" ]
    from_port = 15000
    protocol = "tcp"
    to_port = 15000
  } 

  ingress  {
    cidr_blocks = [ "0.0.0.0/0" ]
    from_port = 8000
    protocol = "tcp"
    to_port = 8000
  } 


  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name: "${var.environment}-default-sg"
  }
}


data "aws_ami" "latest-amazon-linux-2" {
  most_recent = true
  owners = [ "amazon" ]
  filter {
    name = "name"
    values = [var.image_name]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "node_app_ec2" {
  ami = data.aws_ami.latest-amazon-linux-2.id
  instance_type = var.instance_type
  subnet_id = var.subnet_id
  vpc_security_group_ids = [aws_default_security_group.default-SG.id]
  availability_zone = var.avail-zone
  associate_public_ip_address = true 
  key_name = "vockey" 
  tags = {
    Name: "${var.environment}-server"
  }
}

  
